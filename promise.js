

// getUser(1)
//   .then(user => 
//   getRespositories(user.gitHubUsername)
//     .then(repo => 
//       getCommits(repo[0])
//         .then(commit => (
//           console.log(commit)
//         )
//       )
//     )
//   )

  // .then(commit => console.log)


function getCommits(repo) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Get Commits')
      resolve(['commit1', 'commi2', 'commit3'])
    }, 2000);
  })
}

function getRepositories(userName) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Reading a user repo');
      resolve(['repo1', 'repo2', 'repo3'])
    }, 2000);
  })
}

function getUser(id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log('Reading a user from database.');
      resolve({ id: id, gitHubUsername: 'lin' })
    }, 2000);
  })
}

// getUser(1)
//   .then(user => getRepositories(user.gitHubUsername))
//   .then(repo => getCommits(repo[0]))

// promise
// getUser(1)
//   .then(user => getRepositories(user.gitHubUsername))
//   .then(repos => getCommits(repos[0]))
//   .then(commit => console.log(commit))

async function call(){
  try{
    const user = await getUser(1)
    console.log(user);
    const repos = await getRepositories(user.gitHubUsername)
    console.log(repos);
  }catch(ex){
    
  }
}

call();

// callback
// getUser(1, (value) => {
//   getRepositories(value.gitHubUsername, (repos) => {
//     console.log(repos)
//     getCommit()
//   })
//   console.log(value)
// })

// unpending
// before resolve pending
// resolve
// reject

// function getUse(id){
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       console.log('Reading a user from database.');
//       resolve({ id: id, gitHubUsername: 'lin' })
//     }, 2000);
//   })
// }

// function getUser(id, callback) {
//   setTimeout(() => {
//     console.log('Reading a user from database.');
//     callback({ id: id, gitHubUsername: 'lin' })
//   }, 2000);
// }

