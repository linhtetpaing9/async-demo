getUser(1, getRepo)


function getRepo(user){
  console.log(user)
  getRepositories(user.gitHubUsername, getCommit)
}

function getCommit(repo){
  console.log(repo)
  getCommits(repo[0], displayCommits)
}

function displayCommits(commit){
  console.log(commit)
}

function getCommits(repo, callback){
  setTimeout(() => {
    console.log('Get Commits')
   callback([ 'commit1', 'commi2', 'commit3' ]) 
  }, 2000);
}

// getUser(1, user => {
//   console.log(user)
//   getRepositories(user.gitHubUsername, repo => {
//     console.log(repo)
//     getCommits(repo[0], commit => {
//       console.log(commit)
//     })
//   })
// })

function getRepositories(userName, callback) {
  setTimeout(() => {
    console.log('Reading a user repo');
    callback(['repo1', 'repo2', 'repo3'])
  }, 2000);
}

function getCommits(repo, callback) {
  setTimeout(() => {
    console.log('Reading commit');
    callback(['commit1', 'commi2', 'commit3'])
  }, 2000);
}

// Async
function getUser(id, callback) {
  setTimeout(() => {
    console.log('Reading a user from database.');
    callback({ id: id, gitHubUsername: 'lin' })
  }, 2000);
}

function getRepositories(userName, callback) {
  setTimeout(() => {
    console.log('Reading a user repo');
    callback(['repo1', 'repo2', 'repo3'])
  }, 2000);
}

getUser(1, (value) => {
  console.log(value)
  getRepositories(value.gitHubUsername, (repos) => {
    console.log(repos)
    getCommits(repos[0], (commits) => {
      console.log(commits)
    })
  })
})

console.log('Hello')

// Sync
function getUserSync(id) {
  return { id, gitHubUsername: 'Lin' }
}

// console.log(getUserSync(1))